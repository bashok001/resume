%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[]{ashok-resume}
\usepackage{fancyhdr}
\usepackage{ragged2e}

\pagestyle{fancy}
\fancyhf{}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     TITLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{minipage}[c]{0.05\textwidth}
	\raggedright{\drawphoto{./profile}}
\end{minipage}
\begin{minipage}[c]{0.90\textwidth}
	\namesection{{\color{awesome}Ashok}}{Bommisetti}
	{ \locationbold { Senior Engineering Manager | Strategy | Product Development } }
	{ \faLinkedinSquare\space\href{https://www.linkedin.com/in/bashok}{bashok} | \faEnvelope\space\href{mailto:connect@bashok.com}{connect@bashok.com} |  \faGlobe\space\href{https://www.bashok.com}{bashok.com} }
	 {}
\end{minipage} \\
\noindent\makebox[\linewidth]{\color{headings}\rule{\paperwidth}{0.4pt}}
\vspace{-15pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     COLUMN ONE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{minipage}[t]{0.22\textwidth}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     SKILLS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Skills}
\subsection{Programming}
\location{Expert:}
\textbullet{} Java \\
\bigsectionsep
\location{Proficient:}
\textbullet{} Kotlin \textbullet{} Go \textbullet{} Python \\ \textbullet{} Shell Scripting \\
\bigsectionsep
\location{Familiar:}
\textbullet{} Objective C \textbullet{} PL/SQL \textbullet{} \LaTeX

\bigsectionsep

\subsection{Cloud Platforms}
\textbullet{} GCP \textbullet{} AWS

\bigsectionsep

\subsection{Tools}
\textbullet{} Kubernetes \textbullet{} Docker \textbullet{} Terraform \\ \textbullet{} Azkaban

\bigsectionsep

\subsection{Build Tools}
\textbullet{} gradle \textbullet{} Maven \textbullet{} Ant

\bigsectionsep

\subsection{Source Control}
\textbullet{} git \textbullet{} SVN

\bigsectionsep

\subsection{OS}
Linux \textbullet{} Android \textbullet{} Windows

\bigsectionsep

\subsection{Application Software}
\textbullet{} IntelliJ IDE \textbullet{} PyCharm \\ 
\textbullet{} Android Studio  
%\textbullet{} Visual Studio \\

\bigsectionsep

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     EDUCATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Education}

\subsection{Syracuse University}
\descript{M.S., Computer Science}
\location{May 2016 | New York, USA}
\location{Summa Cum Laude}

\bigsectionsep

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Certifications
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{Certifications}
%\subsection{Python Programming}
%\location{March 2015}
%\sectionsep

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Honors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Honors}
\textbullet{} Phi Beta Delta \\
\sectionsep

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     COLUMN TWO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{minipage}
\hfill
\begin{minipage}[t]{0.75\textwidth}

\resumetitle{Seasoned and results-driven engineering leader with over 13 years of experience in engineering and product development. Adept at leading high-performing teams and driving strategic initiatives. My expertise spans fintech, API security, and business software, with a proven track record of fostering innovation and optimizing processes. Passionate about security and privacy-enhancing tools.}
\sectionsep


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     EXPERIENCE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Experience}
\runsubsection{Pleo Technologies}
\descript{|  Senior Engineering Manager, Spend Management}
\location{Aug 2022 – Present | Stockholm, Sweden}
\begin{tightemize}
\item Orchestrated the development of compliance and customer support platforms company-wide, spearheading an engineering delivery strategy that yielded company's highest eNPS scores in the Customer Operations domain.
\item Led the Optimize group's engineering teams, managing team leads and implementing targeted training and coaching to foster a high-performance culture. Achieved a 35\% improvement in velocity, a 26\% reduction in time-to-market for new features, and significantly enhanced cross-functional collaboration.
\item Revamped the hiring pipeline for backend engineering, defining all the stages and processes, resulting in a 14\% reduction in time-to-hire and standardizing the hiring process across all teams.
%\item Championed a seamless customer experience for Subscriptions and Invoices products by fostering an API-first approach in the teams and driving deep integration with Pleo's core product offerings.
\end{tightemize}
\sectionsep

\runsubsection{Klarna Bank AB}
\descript{|  Engineering Manager, Partner Payments}
\location{Oct 2019 – Aug 2022 | Stockholm, Sweden}
\begin{tightemize}
\item Directed two business-critical engineering teams of 12 engineers, ensuring timely delivery of strategic initiatives.
\item Architected and delivered systems enabling the rapid roll-out of new products to millions of merchants across 9 markets within a 3-month timeframe.
\item Led a cross-functional team comprising 5 engineers, 4 analysts, and a product manager in developing fraud detection solutions that prevented losses exceeding 92 million SEK for Klarna in 2022-22.
\item Collaborated with cross-organizational stakeholders, regulators, and authorities to ensure Klarna's global compliance with AML regulations, mitigating risks and maintaining operational integrity.
\end{tightemize}
\sectionsep

\runsubsection{Shape Security}
\descript{|  Team Lead, Defence Intelligence}
\location{Apr 2018 – September 2019 | Mountain View, USA}
\begin{tightemize}
\item Started the new team along with 2 colleagues to work on building a scalable Machine Learning system. Eventually, grew it to two teams of 11 engineers and data scientists.
\item Optimized security response, reducing the reaction time for identified attacks from 6 hours to 35 minutes, while simultaneously cutting operating expenses for rule development by 68\%.
\item Engineered trainers to run a supervised ML system on top of the BigQuery data warehouse, enabling efficient data processing and analysis.
% \item Led the migration of monolithic code bases into scalable, mutually authenticated micro-services running on top of Kubernetes.
%\item Built infrastructure to run ML training and verification with special focus on scalability and availability.
%\item Implemented telemetry emitters for the jobs and built data pipelines that ingest streaming telemetry events to an enriched persistent store.
%\item Provisioned easily managable infrastructure on GCP utilizing IaC(Infrastructure as Code) tools like Terraform.
\end{tightemize}
\sectionsep

\runsubsection{Shape Security}
\descript{\space|  Software Engineer, Mobile}
\location{Jul 2016 – Mar 2018 | Mountain View, USA}
\begin{tightemize}
\item Researching, defining, developing, and iterating on API Protection product with a focus on Android. This SDK currently has 200M+ installations running on over 2500 different models of devices.
%\item Implemented countermeasures to identify fraud using APIs more reliably.
\item Worked on bringing the backend traffic processor into cloud-native world running on Kubernetes.
\item Built a fully automated CI pipeline from scratch to build, test, and deploy four different SDKs connecting Jenkins on the cloud to on-prem test devices.
\end{tightemize}
\sectionsep

\runsubsection{Inkling}
\descript{|  Backend Engineering Intern}
\location{Jul 2015 – Dec 2015 | San Francisco, USA}
\begin{tightemize}
\item Worked on integrating analytics products with Azkaban, an OSS workflow manager.
\item Successfully delivered a project by scaling systems and modifying polling workflows to enable hourly analytics, and built a React-based UI for a v2 dashboard to present these analytics.
\end{tightemize}
\sectionsep

\runsubsection{Digital Ricochet}
\descript{ |  Lead Developer}
\location{Aug 2012 – Dec 2013 | Hyderabad, India}
\begin{tightemize}
\item Built teams from the ground up, driving full product lifecycles from conception to deployment, and developed automation tools that streamlined business flow aggregation across 9 clients.
%\item Developed a tool for Business flow aggregation, integration, and automation that is used by 9 clients.
%\item Built the initial PoC for v1 product, hired the team and drove a full product cycle till customer deployments.
\item Built the infrastructure using various public cloud components to scale between 10 and 200,000 TPS.
\end{tightemize}
\sectionsep

\runsubsection{Accenture Services Pvt Ltd}
\descript{|  Associate Software Engineer}
\location{Jun 2011 – Jul 2012 | Bengaluru, India}
\begin{tightemize}
\item Worked on middleware system of one of the largest telecom companies in the world, with a focus on network integration involving synchronous and asynchronous communications.
\item Involved in the development of a generic proactive monitoring system that is deployed by 4 teams.
%\item Implemented the design of a performance testing automation suite that is currently used across the project.
\end{tightemize}
\sectionsep

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Second Page Setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\end{minipage}
%\newpage
%\begin{minipage}[t]{0.22\textwidth}
%\end{minipage}
%\hfill
%\begin{minipage}[t]{0.75\textwidth}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     RESEARCH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{Research}
%\runsubsection{Cyber Security Research Group}
%\descript{| Research Assistant }
%\location{Jan 2014 – June 2016 | Syracuse, NY}
%\begin{tightemize}
%\item Worked with \textbf{\href{https://scholar.google.com/citations?user=0O4HYVsAAAAJ}{Paul Ratazzi}} and \textbf{\href{http://www.cis.syr.edu/~wedu/}{Dr. Kevin Du}} to create \textbf{PinPoint}, an efficient and near-zero overhead implementation of namespace concept in Android framework.
%\item Design and development of Android launcher with a heavy focus on privacy and security.
%\item Built a complete in-house VPN system that is used by the members of the lab to work from anywhere.
%\item Developed SEED labs, educational labs for cybersecurity, used in over 700 schools across the world.
%\end{tightemize}
%\sectionsep

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PUBLICATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Publications}
\bibliographystyle{plainyr-rev}
\bibliography{publications}
\nocite{*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     AWARDS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{Awards}
%\begin{tabular}{rll}
%2016 & \textbf{Recognized} for valuable contributions by VP of Engineering within 2 months of joining the team \\
%2014 & \textbf{500} out of 5700 competitors in IEEEXtreme 8.0 \\
%2011 - '12 & \textbf{‘Employee of the Month’} by client thrice in 11 months \\
%2012 & \textbf{'Path Finder’} in Annual awards by Accenture for delivering innovative solutions to the client \\
%2011 Q4 & \textbf{'Rising Star’} in Quarterly awards by Accenture for quality delivery in challenging timelines \\
%2007 - '11 & \textbf{'Best student in Technical Innovation’} for 2007-11 batch by Amity University \\
%\end{tabular}
%\sectionsep

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    ACADEMIC PROJECTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Academic Projects}
%\sectionsep
%\runsubsection{Mini VPN} \descript{}
%\location{Jan 2014 – Apr 2014}
%\begin{tightemize}
%\item Prototyped and demonstrated a fully functional SSL VPN with concepts of Crypto, Authentication, Key Management, Key Exchange and Public Key Infrastructure.
%\end{tightemize}
%\sectionsep
%\runsubsection{Intel SGX Emulator} \descript{}
%\location{Aug 2014 – Dec 2014}
%\begin{tightemize}
%\item Implemented a simulator for Intel's newest on-chip security feature, Software Guard Extensions, in software layer, on spec with published documentation.
%\end{tightemize}
%\sectionsep
%\runsubsection{Message Passing Communicator} \descript{}
%\location{Feb 2015 – Apr 2015}
%\begin{tightemize}
%\item Implemented a socket based peer to peer communicator that is capable of syncing trees of files across multiple servers and clients whose messages are modelled after HTTP standards.
%\end{tightemize}

\end{minipage}
\end{document}  \documentclass[]{article}
